from django.urls import resolve
from django.conf import settings


class ChannelPublishMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        url_name = resolve(request.path_info).url_name
        if url_name in settings.PUBLISH_URLS_NAMES:
            pass

        response = self.get_response(request)
        print(response)

        # Code to be executed for each response after view is called.
        return response
